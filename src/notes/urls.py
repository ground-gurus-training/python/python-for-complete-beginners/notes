from django.urls import path

from . import views

app_name = 'notes'
urlpatterns = [
    path('', views.index, name='index'),
    path('note_new', views.note_new, name='note_new'),
    path('<int:note_id>/edit', views.note_edit, name='note_edit'),
    path('<int:note_id>/save', views.note_save, name='note_save'),
    path('<int:note_id>/delete', views.note_delete, name='note_delete'),
]
