from django import forms


class NoteForm(forms.Form):
    id = forms.IntegerField()
    note_text = forms.CharField()
