from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.urls import reverse

from .forms import NoteForm
from .models import Note

# Create your views here.
# controller/request handler


def index(request):
    notes = Note.objects.order_by('id')[:10]
    return render(request, 'index.html', {
        'notes': notes
    })


def note_new(request):
    return render(request, 'note_new.html')


def note_edit(request, note_id):
    try:
        note = Note.objects.get(id=note_id)
    except Note.DoesNotExist as ex:
        print(ex)

    return render(request, 'note_edit.html', {
        'note': note
    })


def note_save(request, note_id):
    if request.method == 'POST':
        form = NoteForm(request.POST)
        note_text = form.data['note_text']

        if note_id == 0:
            note = Note.objects.create(note_text=note_text)
            note.save()
        else:
            try:
                note = Note.objects.get(id=note_id)
                note.note_text = note_text
                note.save()
            except Note.DoesNotExist as ex:
                print(ex)

    return HttpResponseRedirect(reverse('notes:index'))


def note_delete(request, note_id):
    try:
        note = Note.objects.get(id=note_id)
        note.delete()
    except Note.DoesNotExist as ex:
        print(ex)

    return HttpResponseRedirect(reverse('notes:index'))
